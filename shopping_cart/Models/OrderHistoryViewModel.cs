﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace shopping_cart.Models
{
    public class OrderHistoryViewModel
    {
        public OrderHistoryViewModel()
        {
            OrderItems = new List<OrderItemViewModel>();
        }
      [Display(Name = "Order ID")]
        public int Id { get; set; }
        public string Email { get; set; }
        public IList<OrderItemViewModel> OrderItems { get; set; }
    }

    public class OrderItemViewModel
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
    }
}