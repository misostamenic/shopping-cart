﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace shopping_cart.Models
{
    public class CartViewModel
    {
        public CartViewModel()
        {
            SelectedItems = new List<Tbl_Products>();
        }

        public string Email { get; set; }
        public IList<Tbl_Products> SelectedItems { get; set; }
    }
}