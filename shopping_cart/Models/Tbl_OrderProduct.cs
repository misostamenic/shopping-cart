namespace shopping_cart.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Tbl_OrderProduct
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Pk_OrderProdictID { get; set; }

        public int Fk_ProductID { get; set; }

        public int Fk_OrderID { get; set; }

        public virtual Tbl_Orders Tbl_Orders { get; set; }

        public virtual Tbl_Products Tbl_Products { get; set; }
    }
}
