namespace shopping_cart.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ShoppingCartModel : DbContext
    {
        public ShoppingCartModel()
            : base("name=ShoppingCart")
        {
        }

        public virtual DbSet<Tbl_OrderProduct> Tbl_OrderProduct { get; set; }
        public virtual DbSet<Tbl_Orders> Tbl_Orders { get; set; }
        public virtual DbSet<Tbl_Products> Tbl_Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tbl_Orders>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Tbl_Orders>()
                .HasMany(e => e.Tbl_OrderProduct)
                .WithRequired(e => e.Tbl_Orders)
                .HasForeignKey(e => e.Fk_OrderID);

            modelBuilder.Entity<Tbl_Products>()
                .Property(e => e.ProductName)
                .IsUnicode(false);

            modelBuilder.Entity<Tbl_Products>()
                .Property(e => e.ProductPicture)
                .IsUnicode(false);

            modelBuilder.Entity<Tbl_Products>()
                .HasMany(e => e.Tbl_OrderProduct)
                .WithRequired(e => e.Tbl_Products)
                .HasForeignKey(e => e.Fk_ProductID);
        }
    }
}
