﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using shopping_cart.Models;

namespace shopping_cart.Controllers
{
    public class OrderProductController : Controller
    {
        private ShoppingCartModel db = new ShoppingCartModel();

        // GET: OrderProduct
        public ActionResult Index()
        {
            var tbl_OrderProduct = db.Tbl_OrderProduct.Include(t => t.Tbl_Orders).Include(t => t.Tbl_Products);
            return View(tbl_OrderProduct.ToList());
        }

        // GET: OrderProduct/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_OrderProduct tbl_OrderProduct = db.Tbl_OrderProduct.Find(id);
            if (tbl_OrderProduct == null)
            {
                return HttpNotFound();
            }
            return View(tbl_OrderProduct);
        }

        // GET: OrderProduct/Create
        public ActionResult Create()
        {
            ViewBag.Fk_OrderID = new SelectList(db.Tbl_Orders, "Pk_OrderID", "Email");
            ViewBag.Fk_ProductID = new SelectList(db.Tbl_Products, "Pk_ProductID", "ProductName");
            return View();
        }

        // POST: OrderProduct/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Pk_OrderProdictID,Fk_ProductID,Fk_OrderID")] Tbl_OrderProduct tbl_OrderProduct)
        {
            if (ModelState.IsValid)
            {
                db.Tbl_OrderProduct.Add(tbl_OrderProduct);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Fk_OrderID = new SelectList(db.Tbl_Orders, "Pk_OrderID", "Email", tbl_OrderProduct.Fk_OrderID);
            ViewBag.Fk_ProductID = new SelectList(db.Tbl_Products, "Pk_ProductID", "ProductName", tbl_OrderProduct.Fk_ProductID);
            return View(tbl_OrderProduct);
        }

        // GET: OrderProduct/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_OrderProduct tbl_OrderProduct = db.Tbl_OrderProduct.Find(id);
            if (tbl_OrderProduct == null)
            {
                return HttpNotFound();
            }
            ViewBag.Fk_OrderID = new SelectList(db.Tbl_Orders, "Pk_OrderID", "Email", tbl_OrderProduct.Fk_OrderID);
            ViewBag.Fk_ProductID = new SelectList(db.Tbl_Products, "Pk_ProductID", "ProductName", tbl_OrderProduct.Fk_ProductID);
            return View(tbl_OrderProduct);
        }

        // POST: OrderProduct/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Pk_OrderProdictID,Fk_ProductID,Fk_OrderID")] Tbl_OrderProduct tbl_OrderProduct)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_OrderProduct).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Fk_OrderID = new SelectList(db.Tbl_Orders, "Pk_OrderID", "Email", tbl_OrderProduct.Fk_OrderID);
            ViewBag.Fk_ProductID = new SelectList(db.Tbl_Products, "Pk_ProductID", "ProductName", tbl_OrderProduct.Fk_ProductID);
            return View(tbl_OrderProduct);
        }

        // GET: OrderProduct/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_OrderProduct tbl_OrderProduct = db.Tbl_OrderProduct.Find(id);
            if (tbl_OrderProduct == null)
            {
                return HttpNotFound();
            }
            return View(tbl_OrderProduct);
        }

        // POST: OrderProduct/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tbl_OrderProduct tbl_OrderProduct = db.Tbl_OrderProduct.Find(id);
            db.Tbl_OrderProduct.Remove(tbl_OrderProduct);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
