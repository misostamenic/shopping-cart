﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using shopping_cart.Models;

namespace shopping_cart.Controllers
{
    public class OrdersController : Controller
    {
        private ShoppingCartModel db = new ShoppingCartModel();

        // GET: Orders
        public ActionResult Index()
        {
            return View(db.Tbl_Orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Orders tbl_Orders = db.Tbl_Orders.Find(id);
            if (tbl_Orders == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Orders);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Pk_OrderID,Email")] Tbl_Orders tbl_Orders)
        {
            if (ModelState.IsValid)
            {
                db.Tbl_Orders.Add(tbl_Orders);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_Orders);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Orders tbl_Orders = db.Tbl_Orders.Find(id);
            if (tbl_Orders == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Orders);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Pk_OrderID,Email")] Tbl_Orders tbl_Orders)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Orders).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_Orders);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Orders tbl_Orders = db.Tbl_Orders.Find(id);
            if (tbl_Orders == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Orders);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tbl_Orders tbl_Orders = db.Tbl_Orders.Find(id);
            db.Tbl_Orders.Remove(tbl_Orders);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult AddItem(int id)
        {
            var cart = (Session["Cart"] == null) ? new List<int> { } : (List<int>)Session["Cart"];

            cart.Add(id);

            Session["Cart"] = cart;

            return RedirectToAction("Index", "Products", new { });
        }

        [HttpGet]
        public ActionResult Checkout()
        {
            var cart = (Session["Cart"] == null) ? new List<int> { } : (List<int>)Session["Cart"];

            var cartItems = db.Tbl_Products.Where(x => cart.Contains(x.Pk_ProductID)).ToList();

            var model = new CartViewModel
            {
                SelectedItems = cartItems
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Checkout(CartViewModel model)
        {
            if (ModelState.IsValid)
            {
                var order = new Tbl_Orders {
                    Email = model.Email
                };

                foreach(var item in model.SelectedItems)
                {
                    var orderItem = new Tbl_OrderProduct
                    {
                        Fk_OrderID = order.Pk_OrderID,
                        Fk_ProductID = item.Pk_ProductID
                    };

                    order.Tbl_OrderProduct.Add(orderItem);
                }

                db.Tbl_Orders.Add(order);
                db.SaveChanges();

                ModelState.Clear();

                Session["Cart"] = null;

                return RedirectToAction("Index", "Products");              
               
            }
            return View(new CartViewModel()); 
        }

        public ActionResult OrderHistory()
        {        
            List<OrderHistoryViewModel> OrderHistoryList = new List<OrderHistoryViewModel>();

            var allOrders = db.Tbl_Orders.ToList();

            foreach(var item in allOrders)
            {
                var order = new OrderHistoryViewModel
                {
                    Id = item.Pk_OrderID,
                    Email = item.Email,
                    OrderItems = item.Tbl_OrderProduct.Select(x => new OrderItemViewModel
                    {
                        Id = x.Pk_OrderProdictID,
                        ProductName = x.Tbl_Products.ProductName
                    }).ToList()
                };

                OrderHistoryList.Add(order);
            }

            return View(OrderHistoryList);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
