﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(shopping_cart.Startup))]
namespace shopping_cart
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
