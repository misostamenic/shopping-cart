# Set params
$databaseName = "$OctopusEnvironmentName.cart.loc".ToLower()
$projectName = "shopping_cart_DB"

# Add the DLL
Add-Type -path "C:\Program Files (x86)\Microsoft SQL Server\120\DAC\bin\Microsoft.SqlServer.Dac.dll"

# Create the connection strnig
$connectionString = "server=(local)"

$d = New-Object Microsoft.SqlServer.Dac.DacServices $connectionString

$dacpac = (Get-Location).Path + "\Content\" + $projectName + ".dacpac"

$dacpacXml = (Get-Location).Path + "\Content\" + $databaseName + ".publish.xml"

Write-Host $dacpac
Write-Host $dacpacXml

#Read a publish profile XML to get the deployment options
$dacProfile = [Microsoft.SqlServer.Dac.DacProfile]::Load($dacpacXml)

# Load dacpac from file & deploy to database
$dp = [Microsoft.SqlServer.Dac.DacPackage]::Load($dacpac)

# Generate the deplopyment script
$deployScriptName = $databaseName + ".sql"
$deployScript = $d.GenerateDeployScript($dp, $dacProfile.TargetDatabaseName, $dacProfile.DeployOptions)

# Return the script to the log
Write-Host $deployScript

# Write the script out to a file
$deployScript | Out-File $deployScriptName

# Deploy the dacpac
$d.Deploy($dp, $dacProfile.TargetDatabaseName, $true, $dacProfile.DeployOptions)
