﻿CREATE TABLE [dbo].[Tbl_Orders] (
    [Pk_OrderID] INT          IDENTITY (1, 1) NOT NULL,
    [Email]      VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Tbl_Orders] PRIMARY KEY CLUSTERED ([Pk_OrderID] ASC)
);

