﻿CREATE TABLE [dbo].[Tbl_Products] (
    [Pk_ProductID]   INT          IDENTITY (1, 1) NOT NULL,
    [ProductName]    VARCHAR (50) NOT NULL,
    [ProductPicture] VARCHAR (50) NULL,
    CONSTRAINT [PK_Tbl_Products] PRIMARY KEY CLUSTERED ([Pk_ProductID] ASC)
);

