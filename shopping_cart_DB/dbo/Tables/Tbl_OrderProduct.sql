﻿CREATE TABLE [dbo].[Tbl_OrderProduct] (
    [Pk_OrderProdictID] INT IDENTITY (1, 1) NOT NULL,
    [Fk_ProductID]      INT NOT NULL,
    [Fk_OrderID]        INT NOT NULL,
    CONSTRAINT [PK_Tbl_OrderProduct] PRIMARY KEY CLUSTERED ([Pk_OrderProdictID] ASC),
    CONSTRAINT [FK_Tbl_OrderProduct_Tbl_Orders] FOREIGN KEY ([Fk_OrderID]) REFERENCES [dbo].[Tbl_Orders] ([Pk_OrderID]),
    CONSTRAINT [FK_Tbl_OrderProduct_Tbl_Products] FOREIGN KEY ([Fk_ProductID]) REFERENCES [dbo].[Tbl_Products] ([Pk_ProductID])
);

