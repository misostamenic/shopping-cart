﻿CREATE LOGIN [shopping_cart_admin] 
	WITH PASSWORD = 'shopping_cart_admin', DEFAULT_DATABASE = [$(DatabaseName)], DEFAULT_LANGUAGE = [us_english], CHECK_POLICY = OFF, CHECK_EXPIRATION = OFF;
